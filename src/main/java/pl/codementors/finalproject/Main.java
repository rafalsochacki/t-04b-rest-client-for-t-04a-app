package pl.codementors.finalproject;

import pl.codementors.finalproject.workers.ContinousConsumingWorker;

import java.util.Scanner;

/**
 * Thread to handle REST from web application
 *
 * @author rafal
 */
public class Main {
    public static void main(String[] args) {
        startContinousConsumingWorker();
    }

    /**
     * Method to start worker.
     */
    private static void startContinousConsumingWorker() {

        ContinousConsumingWorker consumingWorker = new ContinousConsumingWorker();

        Thread thread = new Thread(consumingWorker);

        thread.start();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.equals("quit")) {
                break;
            }
        }

        consumingWorker.stop();

        thread.interrupt();

    }

}
