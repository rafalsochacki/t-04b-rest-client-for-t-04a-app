package pl.codementors.finalproject.workers;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Worker consuming some resource. It works continuously until it is stopped.
 *
 * @author rafal
 */
public class ContinousConsumingWorker implements Runnable {

    private static final Logger log = Logger.getLogger(ContinousConsumingWorker.class.getCanonicalName());

    /**
     * Thread should continue running as long as this var is set to true.
     */
    boolean running = true;

    /**
     * Resource container.
     */
    private ConsumableResource resource = new ConsumableResource();

    @Override
    public void run() {
        try {
            resource.getProductsList().forEach(System.out::println);
            while (running) {
                resource.updateList();
                Thread.sleep(6000);

            }
        } catch (InterruptedException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }
    }

    /**
     * Stops this worker
     */
    public void stop() {
        running = false;
    }

}
