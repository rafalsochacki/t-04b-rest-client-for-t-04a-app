package pl.codementors.finalproject.workers;


import pl.codementors.finalproject.models.Product;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Container where resource can be put inside and taken outside
 *
 * @author rafal
 */
public class ConsumableResource {

    public List<Product> productsList;

    /**
     * @return products list which can be use outside of this class.
     */
    public List<Product> getProductsList() {
        return productsList = getAllProducts();
    }

    /**
     * @return list of product which already are in the database.
     */
    public List<Product> getAllProducts() {

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/security/api/products");
        Invocation.Builder request = target.request();
        Response response = request.get();
        return response.readEntity(new GenericType<List<Product>>() {});

    }

    /**
     * Updates printed out list of products.
     */
    public void updateList() {
        List<Product> productList = getAllProducts();
        List<Product> newListings = productList.stream().filter(product -> !productsList.contains(product)).collect(Collectors.toList());
        productsList = productList;
        newListings.forEach(System.out::println);
    }
}
