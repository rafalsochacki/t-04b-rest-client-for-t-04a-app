package pl.codementors.finalproject.models;

import java.util.Objects;

/**
 * Single product representation
 *
 * @author rafal
 */
public class Product {

    /**
     * Product id.
     */
    private int id;

    /**
     * Product name.
     */
    private String name;

    /**
     * Product seller.
     */
    private User seller;

    /**
     * Product price.
     */
    private int price;

    /**
     * Product availability.
     */
    private boolean available;

    public Product() {
    }

    public Product(int id, String name, User seller, int price, boolean available) {
        this.id = id;
        this.name = name;
        this.seller = seller;
        this.price = price;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                price == product.price &&
                available == product.available &&
                Objects.equals(name, product.name) &&
                Objects.equals(seller, product.seller);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, seller, price, available);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", seller=" + seller +
                ", price=" + price +
                ", available=" + available +
                '}';
    }
}
